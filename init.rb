# Copyright (C) 2019  Sutou Kouhei <kou@clear-code.com>
# Copyright (C) 2019  Shimadzu Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Redmine::Plugin.register :delayed_job do
  name "Delayed Job plugin"
  author "Sutou Kouhei, Shimadzu Corporation"
  description "This is a Redmine plugin to enable Delayed Job as Active Job backend"
  version "1.0.1"
  url "https://gitlab.com/redmine-plugin-delayed-job/redmine-plugin-delayed-job"
  author_url "https://gitlab.com/redmine-plugin-delayed-job"
  directory __dir__
end

enable = (Rails.env != "test")
case ENV["REDMINE_PLUGIN_DELAYED_JOB_ENABLE"]
when "yes"
  enable = true
when "no"
  enable = false
end
if enable
  if ActiveRecord::Base.respond_to?(:connection_db_config)
    adapter = ActiveRecord::Base.connection_db_config.adapter
  else
    adapter = ActiveRecord::Base.connection_config[:adapter]
  end
  case adapter
  when "mysql2"
    Delayed::Backend::ActiveRecord.configure do |config|
      config.reserve_sql_strategy = :default_sql
    end
  end
  ActiveJob::Base.queue_adapter = ActiveJob::QueueAdapters::DelayedJobAdapter.new
  if ENV["REDMINE_PLUGIN_DELAYED_JOB_STOP_BY_TMP_RESTART_TXT"] == "yes"
    class RedminePluginDelayedJobStopByTmpRestartTxtPlugin < Delayed::Plugin
      callbacks do |lifecycle|
        start_time = Time.now
        tmp_restart_txt = Rails.root + "tmp/restart.txt"
        lifecycle.after(:loop) do |worker|
          if tmp_restart_txt.exist? and tmp_restart_txt.stat.mtime > start_time
            worker.say("Exiting by tmp/restart.txt...")
            worker.stop
          end
        end
      end
    end
    Delayed::Worker.plugins << RedminePluginDelayedJobStopByTmpRestartTxtPlugin
  end
end
